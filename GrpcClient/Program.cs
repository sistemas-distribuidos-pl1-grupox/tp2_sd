﻿using Grpc.Net.Client;
using GrpcServer;
using GrpcServer.Protos;
using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

namespace GrpcClient
{
    static class Program
    {
        static async Task Main(string[] args)
        {

            var channel = GrpcChannel.ForAddress("https://localhost:7161");
            var client = new Provisioning.ProvisioningClient(channel);

            bool exit = false;

            while (!exit)
            {
                Console.WriteLine("Lista de processos:");
                Console.WriteLine("1. Reserva");
                Console.WriteLine("2. Ativação");
                Console.WriteLine("3. Desativação");
                Console.WriteLine("4. Término");
                Console.WriteLine("5. Mostrar as minhas operações");
                Console.WriteLine("6. Mostrar domicílios disponíveis (admin)");
                Console.WriteLine("7. Mostrar operações realizadas (admin)");
                Console.WriteLine("8. Exit");

                int choice = int.Parse(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Reserva: \n");
                        InvokeReservarServiceMethod(client);
                        break;
                    case 2:
                        Console.WriteLine("Ativação: \n");
                        InvokeAtivarServiceMethod(client);
                        break;
                    case 3:
                        Console.WriteLine("Desativação: \n");
                        InvokeDesativarServiceMethod(client);
                        break;
                    case 4:
                        Console.WriteLine("Terminação: \n");
                        InvokeTerminoServiceMethod(client);
                        break;
                    case 5:
                        Console.WriteLine("As minhas operações: \n");
                        InvokeMyInfo(client);
                        break;
                    case 6:
                        Console.WriteLine("Domicílios disponíveis: \n");
                        InvokeFreeCoverage(client);
                        break;
                    case 7:
                        Console.WriteLine("Todas as operações: \n");
                        InvokeAllInfo(client);
                        break;
                    case 8:
                        exit = true;
                        break;
                    default:
                        Console.WriteLine("Invalid option selected.");
                        break;
                }

                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
                Console.Clear();
            }
        }

        private static void InvokeReservarServiceMethod(Provisioning.ProvisioningClient client)
        {
            Console.WriteLine("Digite o username:");
            string userId = Console.ReadLine();

            Console.WriteLine("Digite a senha:");
            string password = Console.ReadLine();

            Console.WriteLine("Digite a rua:");
            string rua = Console.ReadLine();

            Console.WriteLine("Digite o número:");
            string numeroInput = Console.ReadLine();
            int numero = int.Parse(numeroInput);


            var request = new ReserveRequest
            {
                UserID = userId,
                Passw = password,
                Rua = rua,
                Num = numero
            };

            var response = client.Reserve(request);

            Console.WriteLine(response.Result);
            Console.WriteLine("Número Administrativo: " + response.NumAdministrativo);

            Console.ReadLine();
        }

        private static void InvokeAtivarServiceMethod(Provisioning.ProvisioningClient client)
        {
            Console.WriteLine("Digite o username:");
            string userId = Console.ReadLine();

            Console.WriteLine("Digite a senha:");
            string password = Console.ReadLine();

            Console.WriteLine("Digite o número administrativo:");
            string numInput = Console.ReadLine();
            int num_administrativo = int.Parse(numInput);

            var request = new ActivateRequest
            {
                UserID = userId,
                Passw = password,
                NumAdministrativo = num_administrativo
            };

            var response = client.Activate(request);

            Console.WriteLine(response.Result);
            Console.WriteLine("Tempo estimado: " + response.ExpectedActivationTime + " segundos");

            if (response.CanActivate == true)
            {
                // Inscrever-se no tópico EVENTS do RabbitMQ
                //FALTA PROGRAMAR ESTEE MÉTODO
                SubscribeToRabbitMQ(userId);
            }

            Console.ReadLine();

           
        }

        private static void InvokeDesativarServiceMethod(Provisioning.ProvisioningClient client)
        {
            Console.WriteLine("Digite o username:");
            string userId = Console.ReadLine();

            Console.WriteLine("Digite a senha:");
            string password = Console.ReadLine();

            Console.WriteLine("Digite o número administrativo:");
            string numInput = Console.ReadLine();
            int num_administrativo = int.Parse(numInput);

            var request = new DeactivateRequest
            {
                UserID = userId,
                Passw = password,
                NumAdministrativo = num_administrativo
            };

            var response = client.Deactivate(request);

            Console.WriteLine(response.Result);
            Console.WriteLine("Tempo estimado: " + response.ExpectedActivationTime + " segundos");

            if (response.CanActivate == true)
            {
                // Inscrever-se no tópico EVENTS do RabbitMQ
                //FALTA PROGRAMAR ESTEE MÉTODO
                SubscribeToRabbitMQ(userId);
            }

            Console.ReadLine();
        }

        private static void InvokeTerminoServiceMethod(Provisioning.ProvisioningClient client)
        {
            Console.WriteLine("Digite o username:");
            string userId = Console.ReadLine();

            Console.WriteLine("Digite a senha:");
            string password = Console.ReadLine();

            Console.WriteLine("Digite o número administrativo:");
            string numInput = Console.ReadLine();
            int num_administrativo = int.Parse(numInput);

            var request = new TerminateRequest
            {
                UserID = userId,
                Passw = password,
                NumAdministrativo = num_administrativo
            };

            var response = client.Terminate(request);

            Console.WriteLine(response.Result);
            Console.WriteLine("Tempo estimado: " + response.ExpectedActivationTime + " segundos");

            if (response.CanActivate == true)
            {
                // Inscrever-se no tópico EVENTS do RabbitMQ
                //FALTA PROGRAMAR ESTEE MÉTODO
               SubscribeToRabbitMQ(userId);
            }

            Console.ReadLine();
        }

        private static void InvokeMyInfo(Provisioning.ProvisioningClient client)
        {
            Console.WriteLine("Digite o username:");
            string userId = Console.ReadLine();

            Console.WriteLine("Digite a senha:");
            string password = Console.ReadLine();


            var request = new MyInfoRequest
            {
                UserID = userId,
                Passw = password
            };

            var response = client.MyInfo(request);

            Console.WriteLine(response.Result);
            //Console.WriteLine("Tempo estimado: " + response.ExpectedActivationTime + " segundos");
            if (response.Result == "Success")
            {
                foreach (var operacaoMessage in response.Operacoes)
                {
                    Console.WriteLine($"Operacao ID: {operacaoMessage.Id}, Operator: {operacaoMessage.Operator}, Operacao: {operacaoMessage.Operacao}");
                }
            }

            Console.ReadLine();
        }

        private static void InvokeAllInfo(Provisioning.ProvisioningClient client)
        {
            Console.WriteLine("Digite o username:");
            string userId = Console.ReadLine();

            Console.WriteLine("Digite a senha:");
            string password = Console.ReadLine();


            var request = new AllInfoRequest
            {
                UserID = userId,
                Passw = password
            };

            var response = client.AllInfo(request);

            Console.WriteLine(response.Result);
            //Console.WriteLine("Tempo estimado: " + response.ExpectedActivationTime + " segundos");
            if (response.Result == "Success")
            {
                foreach (var operacaoMessage in response.Operacoes)
                {
                    Console.WriteLine($"Operacao ID:{operacaoMessage.Id}, Operator:{operacaoMessage.Operator}, Operacao:{operacaoMessage.Operacao}");
                }
            }

            Console.ReadLine();
        }

        private static void InvokeFreeCoverage(Provisioning.ProvisioningClient client)
        {
            Console.WriteLine("Digite o username:");
            string userId = Console.ReadLine();

            Console.WriteLine("Digite a senha:");
            string password = Console.ReadLine();


            var request = new FreeCoverageRequest
            {
                UserID = userId,
                Passw = password
            };

            var response = client.FreeCoverage(request);

            if (response.Result == "Success")
            {
                foreach (var coverageMessage in response.Coverages)
                {
                    Console.WriteLine($"Número administrativo: {coverageMessage.Numadm},Rua:{coverageMessage.Rua},Número:{coverageMessage.Nume}, Estado: {coverageMessage.State}");
                }
            }

            Console.ReadLine();
        }

        private static async Task SubscribeToRabbitMQ(string userId)
        {
            await Task.Delay(TimeSpan.FromSeconds(10));
            var factory = new ConnectionFactory()
            {
                HostName = "localhost",
                Port = 5672,
                VirtualHost = "/",
                UserName = "grupo1",
                Password = "grupo1"
            };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: $"EVENTS_{userId}",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine($"Received: {message}");
                };

                channel.BasicConsume(queue: $"EVENTS_{userId}",
                                     autoAck: true,
                                     consumer: consumer);

                Console.ReadLine();
                Console.Clear();
            }
        }
    }
}
