﻿using System;
using System.Collections.Generic;
using GrpcServer.Models;
using Microsoft.EntityFrameworkCore;

namespace GrpcServer.Data;

public partial class SDfinalDatabaseContext : DbContext
{
    public SDfinalDatabaseContext()
    {
    }

    public SDfinalDatabaseContext(DbContextOptions<SDfinalDatabaseContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Coverage> Coverages { get; set; }

    public virtual DbSet<Operaco> Operacoes { get; set; }

    public virtual DbSet<User> Users { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseSqlServer("name=SDfinalDatabaseContext");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Coverage>(entity =>
        {
            entity.HasKey(e => e.NumAdministrativo).HasName("PK__Coverage__F37EB4978306F78A");

            entity.Property(e => e.NumAdministrativo).ValueGeneratedNever();
        });

        modelBuilder.Entity<Operaco>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Operacoe__3213E83F240AE940");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Users__3213E83F8A2C33C3");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
