using Grpc.Core;
using GrpcServer;
using GrpcServer.Data;
using GrpcServer.Models;
using GrpcServer.Protos;
using Microsoft.EntityFrameworkCore;
using RabbitMQ.Client;
using System.Text;

namespace GrpcServer.Services
{
    public class ProvisioningService : Provisioning.ProvisioningBase
    {
        private readonly SDfinalDatabaseContext _dbContext;
        private readonly ILogger<ProvisioningService> _logger;

        public ProvisioningService(ILogger<ProvisioningService> logger, SDfinalDatabaseContext dBContext)
        {
            _dbContext = dBContext;
            _logger = logger;
        }

        #region Servi�o de Reserva
        public override Task<ReserveReply> Reserve(ReserveRequest request, ServerCallContext context)
        {
            try
            {
                var user = _dbContext.Users.FirstOrDefault(u => u.Username == request.UserID);
                if (user == null || user.Password != request.Passw)
                {
                    return Task.FromResult(new ReserveReply { Result = "Usu�rio ou senha incorretos." });
                }
                else
                {
                    var domicilio = _dbContext.Coverages.FirstOrDefault(d => d.Rua == request.Rua && d.Numero == request.Num);
                    if (domicilio == null || domicilio.Estado != "FREE")
                    {
                        return Task.FromResult(new ReserveReply { Result = "Domic�lio n�o dispon�vel para reserva." });
                    }

                    domicilio.Operator = user.Operator;
                    domicilio.Estado = "RESERVED";
                    _dbContext.Coverages.Update(domicilio);
                    _dbContext.SaveChanges();

                    var operacao = new Operaco
                    {
                        Operacao = "RESERVATION",
                        Operador = user.Operator,
                        NumAdministrativo = domicilio.NumAdministrativo,
                        Dataatual = DateTime.UtcNow
                    };
                    _dbContext.Operacoes.Add(operacao);

                    _dbContext.SaveChanges();

                    return Task.FromResult(new ReserveReply { Result = "Reserva efetuada com sucesso.", NumAdministrativo = domicilio.NumAdministrativo });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");

                throw new RpcException(new Status(StatusCode.Internal, "An error occurred while processing the request"));
            }
        }
        #endregion

        #region Servi�o de Ativa��o
        public override async Task<ActivateReply> Activate(ActivateRequest request, ServerCallContext context)
        {
            try
            {
                var user = _dbContext.Users.FirstOrDefault(u => u.Username == request.UserID);
                if (user == null || user.Password != request.Passw)
                {
                    return new ActivateReply { Result = "Usu�rio ou senha incorretos.", CanActivate = false};
                }
                else
                {
                    var domicilio = _dbContext.Coverages.FirstOrDefault(d => d.NumAdministrativo == request.NumAdministrativo);
                    if (domicilio == null || (domicilio.Estado != "RESERVED" && domicilio.Estado != "DEACTIVATED") || domicilio.Operator != user.Operator)
                    {
                        return new ActivateReply { Result = "Domic�lio n�o dispon�vel para ativa��o.", CanActivate = false};
                    }
                    int estimatedTime = 5; // Tempo estimado em segundos
                    await ActivateServiceAsync(user, domicilio, estimatedTime);
                    return new ActivateReply { Result = "Ativa��o iniciada", ExpectedActivationTime = estimatedTime, CanActivate = true};
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");

                throw new RpcException(new Status(StatusCode.Internal, "An error occurred while processing the request"));
            }
        }
        private async Task ActivateServiceAsync(User user, Coverage coverage, int estimatedTime)
        {
            // Simular tempo de ativa��o
            // Este delay deve estar dentro do PublishMQ (acho eu)
            //await Task.Delay(TimeSpan.FromSeconds(estimatedTime));

            // Atualizar estado da cobertura
            coverage.Estado = "ACTIVATED";
            _dbContext.Coverages.Update(coverage);
            await _dbContext.SaveChangesAsync();

            // Adicionar linha � tabela Operacoes
            var operacao = new Operaco
            {
                Operacao = "ACTIVATION",
                Operador = user.Operator,
                NumAdministrativo = coverage.NumAdministrativo,
                Dataatual = DateTime.UtcNow
            };
            _dbContext.Operacoes.Add(operacao);
            await _dbContext.SaveChangesAsync();

            // Publicar mensagem no t�pico EVENTS do RabbitMQ
            string activationMessage = $"Servi�o ativado para o utilizador {user.Username}, com n�mero administrativo {coverage.NumAdministrativo}.";
            await PublishToRabbitMQ(user.Username, activationMessage);
        }
        #endregion

        #region Servi�o de Desativa��o
        public override async Task<DeactivateReply> Deactivate(DeactivateRequest request, ServerCallContext context)
        {
            try
            {
                var user = _dbContext.Users.FirstOrDefault(u => u.Username == request.UserID);
                if (user == null || user.Password != request.Passw)
                {
                    return new DeactivateReply { Result = "Usu�rio ou senha incorretos.", CanActivate = false};
                }
                else
                {
                    var domicilio = _dbContext.Coverages.FirstOrDefault(d => d.NumAdministrativo == request.NumAdministrativo);
                    if (domicilio == null || domicilio.Estado != "ACTIVATED" || domicilio.Operator != user.Operator)
                    {
                        return new DeactivateReply { Result = "Domic�lio n�o dispon�vel para desativa��o.", CanActivate = false};

                    }
                    int estimatedTime = 5; // Tempo estimado em segundos
                    await DeactivateServiceAsync(user, domicilio, estimatedTime);
                    return new DeactivateReply { Result = "Desativa��o iniciada", ExpectedActivationTime = estimatedTime, CanActivate = true};
                }
            }
            catch (Exception ex)
            {
                // Log the exception or return an appropriate error message to the client
                Console.WriteLine($"An error occurred: {ex.Message}");

                throw new RpcException(new Status(StatusCode.Internal, "An error occurred while processing the request"));
            }
        }
        private async Task DeactivateServiceAsync(User user, Coverage coverage, int estimatedTime)
        {
            // Simular tempo de ativa��o
            await Task.Delay(TimeSpan.FromSeconds(estimatedTime));

            // Atualizar estado da cobertura
            coverage.Estado = "DEACTIVATED";
            _dbContext.Coverages.Update(coverage);
            await _dbContext.SaveChangesAsync();

            // Adicionar linha � tabela Operacoes
            var operacao = new Operaco
            {
                Operacao = "DEACTIVATION",
                Operador = user.Operator,
                NumAdministrativo = coverage.NumAdministrativo,
                Dataatual = DateTime.UtcNow
            };
            _dbContext.Operacoes.Add(operacao);
            await _dbContext.SaveChangesAsync();

            // Publicar mensagem no t�pico EVENTS do RabbitMQ
            string activationMessage = $"Servi�o desativado para o utilizador {user.Username}, com n�mero administrativo {coverage.NumAdministrativo}.";
            await PublishToRabbitMQ(user.Username, activationMessage);
        }
        #endregion

        #region Servi�o de T�rmino
        public override async Task<TerminateReply> Terminate(TerminateRequest request, ServerCallContext context)
        {
            try
            {
                var user = _dbContext.Users.FirstOrDefault(u => u.Username == request.UserID);
                if (user == null || user.Password != request.Passw)
                {
                    return new TerminateReply { Result = "Usu�rio ou senha incorretos.", CanActivate = false};
                }
                else
                {
                    var domicilio = _dbContext.Coverages.FirstOrDefault(d => d.NumAdministrativo == request.NumAdministrativo);
                    if (domicilio == null || domicilio.Estado != "DEACTIVATED" || domicilio.Operator != user.Operator)
                    {
                        return new TerminateReply { Result = "Domic�lio n�o dispon�vel para t�rmino." , CanActivate = false };

                    }
                    int estimatedTime = 5; // Tempo estimado em segundos
                    await TerminationServiceAsync(user, domicilio, estimatedTime);
                    return new TerminateReply { Result = "T�rmino iniciado", ExpectedActivationTime = estimatedTime, CanActivate = true};
                }
            }
            catch (Exception ex)
            {
                // Log the exception or return an appropriate error message to the client
                Console.WriteLine($"An error occurred: {ex.Message}");

                throw new RpcException(new Status(StatusCode.Internal, "An error occurred while processing the request"));
            }
        }
        private async Task TerminationServiceAsync(User user, Coverage coverage, int estimatedTime)
        {
            // Simular tempo de ativa��o
            await Task.Delay(TimeSpan.FromSeconds(estimatedTime));

            // Atualizar estado da cobertura
            coverage.Estado = "FREE";
            coverage.Operator = null;
            coverage.Modalidade = null;

            _dbContext.Coverages.Update(coverage);
            await _dbContext.SaveChangesAsync();

            // Adicionar linha � tabela Operacoes
            var operacao = new Operaco
            {
                Operacao = "TERMINATION",
                Operador = user.Operator,
                NumAdministrativo = coverage.NumAdministrativo,
                Dataatual = DateTime.UtcNow
            };
            _dbContext.Operacoes.Add(operacao);
            await _dbContext.SaveChangesAsync();

            // Publicar mensagem no t�pico EVENTS do RabbitMQ
            string activationMessage = $"Servi�o terminado para o utilizador {user.Username}, com n�mero administrativo {coverage.NumAdministrativo}.";
            await PublishToRabbitMQ(user.Username, activationMessage);
        }
        #endregion

        public override async Task<MyInfoReply> MyInfo(MyInfoRequest request, ServerCallContext context)
        {
            var user = _dbContext.Users.FirstOrDefault(u => u.Username == request.UserID);
            if (user == null || user.Password != request.Passw)
            {
                return new MyInfoReply { Result = "Usu�rio ou senha incorretos."};
            }
            else
            {
                // Get the Operacoes records where user.Operator is equal to Operacoes.Operator
                var operacoes = await _dbContext.Operacoes.Where(o => o.Operador == user.Operator).ToListAsync();

                // Convert the Operacoes records to OperacoMessage objects
                var operacoesMessages = operacoes.Select(o => new OperacoMessage
                {
                    Id = o.Id,
                    Operator = o.Operador,
                    Operacao = o.Operacao
                    // Map other fields from the Operaco class as needed
                }).ToList();

                // Return the result
                return new MyInfoReply
                {
                    Result = "Success",
                    Operacoes = { operacoesMessages }
                };
            }
        }

        public override async Task<AllInfoReply> AllInfo(AllInfoRequest request, ServerCallContext context)
        {
            var user = _dbContext.Users.FirstOrDefault(u => u.Username == request.UserID);
            if (user == null || user.Password != request.Passw || user.Operator != "OWNER")
            {
                return new AllInfoReply { Result = "Usu�rio ou senha incorretos." };
            }
            else
            {
                // Get all the Operacoes records
                var operacoes = await _dbContext.Operacoes.ToListAsync();

                // Convert the Operacoes records to OperacoMessage objects
                var operacoesMessages = operacoes.Select(o => new OperacoMessage
                {
                    Id = o.Id,
                    Operator = o.Operador,
                    Operacao = o.Operacao
                    // Map other fields from the Operaco class as needed
                }).ToList();

                // Return the result
                return new AllInfoReply
                {
                    Result = "Success",
                    Operacoes = { operacoesMessages }
                };
            }
        }

        public override async Task<FreeCoverageReply> FreeCoverage(FreeCoverageRequest request, ServerCallContext context)
        {
            var user = _dbContext.Users.FirstOrDefault(u => u.Username == request.UserID);
            if (user == null || user.Password != request.Passw || user.Operator != "OWNER")
            {
                return new FreeCoverageReply { Result = "Usu�rio ou senha incorretos." };
            }
            else
            {
                // Get all the Coverage records with state = FREE
                var freeCoverages = await _dbContext.Coverages.Where(c => c.Estado == "FREE").ToListAsync();

                // Convert the Coverage records to CoverageMessage objects
                var coverageMessages = freeCoverages.Select(c => new CoverageMessage
                {
                    Numadm = c.NumAdministrativo,
                    Rua = c.Rua,
                    Nume = (int)c.Numero,
                    State = c.Estado
                }).ToList();

                // Return the result
                return new FreeCoverageReply
                {
                    Result = "Success",
                    Coverages = { coverageMessages }
                };
            }
        }

        private async Task PublishToRabbitMQ(string userId, string message)
        {

            var factory = new ConnectionFactory()
            {
                HostName = "localhost",
                Port = 5672,
                VirtualHost = "/",
                UserName = "grupo1",
                Password = "grupo1"
            };
            try
            {
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: $"EVENTS_{userId}",
                                         durable: false,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    var body = Encoding.UTF8.GetBytes(message);


                    channel.BasicPublish(exchange: "",
                                         routingKey: $"EVENTS_{userId}",
                                         basicProperties: null,
                                         body: body);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error in PublishToRabbitMQ: {ex.Message}");
            }
        }

    }
}
